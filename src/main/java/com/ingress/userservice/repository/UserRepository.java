package com.ingress.userservice.repository;

import com.ingress.userservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository  extends JpaRepository<User,Integer> {
}
