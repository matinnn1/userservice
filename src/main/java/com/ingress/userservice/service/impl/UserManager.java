package com.ingress.userservice.service.impl;

import com.ingress.userservice.dto.UserDto;
import com.ingress.userservice.entity.User;
import com.ingress.userservice.repository.UserRepository;
import com.ingress.userservice.service.inter.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserManager implements UserService {
    private final UserRepository userRepository;

    public UserManager(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDto> getALl() {
        List<User> users = userRepository.findAll();
        return users.stream().map((user) -> mapToUserDto(user)).collect(Collectors.toList());
    }

    public UserDto addUser(UserDto userDto) {

        User createdUser = userRepository.save(mapToUser(userDto));

        return mapToUserDto(createdUser);
    }

    public UserDto getById(int id) {
        var user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));

        return mapToUserDto(user);
    }

    public UserDto updateUser(int id, UserDto userDto) {

        var user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));

        user = mapToUser(userDto);

        user = userRepository.save(user);

        return mapToUserDto(user);
    }

    public void deleteUserById(int id) {
        userRepository.deleteById(id);
    }

    private UserDto mapToUserDto(User user) {
        UserDto userDto = UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .age(user.getAge())
                .build();

        return userDto;
    }

    private User mapToUser(UserDto userDto) {
        User user = User.builder()
                .id(userDto.getId())
                .name(userDto.getName())
                .email(userDto.getEmail())
                .age(userDto.getAge())
                .build();

        return user;
    }
}
