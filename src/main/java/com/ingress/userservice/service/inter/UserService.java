package com.ingress.userservice.service.inter;

import com.ingress.userservice.dto.UserDto;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface UserService {
    List<UserDto> getALl();
    UserDto addUser(UserDto userDto);
    UserDto getById(int id);
    UserDto updateUser(int id, UserDto userDto);
    void deleteUserById(int id);
}
